Rails.application.routes.draw do
  root to: 'products#index'
  resources :products

  namespace :api do
    namespace :v1 do
      resources :products, only: [:index, :create, :show, :update, :destroy]
    end
  end
end
