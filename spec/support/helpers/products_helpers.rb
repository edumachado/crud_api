module Features
  module ProductsHelpers
    def fill_form_with(description, price)
      visit root_path
      click_link 'New Product'
      fill_in 'Description', with: description
      fill_in 'Price', with: price
      click_button 'Create Product'
    end
  end
end
