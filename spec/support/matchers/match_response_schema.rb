RSpec::Matchers.define :match_response_schema do |schema|
  match do |response|
    schema_path = Rails.root.join('spec/support/schemas/api', "#{schema}.json").to_s

    JSON::Validator.validate!(schema_path, JSON.parse(response.body)['data'], strict: true)
  end
end
