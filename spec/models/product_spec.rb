require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_presence_of(:price) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:price).of_type(:decimal) }
    it { is_expected.to have_db_column(:description).of_type(:string) }
  end

  context 'factories' do
    it { expect(build(:product)).to be_valid }
    it { expect(build(:invalid_product)).to_not be_valid }
  end
end
