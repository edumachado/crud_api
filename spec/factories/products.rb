FactoryGirl.define do
  factory :product do
    description { FFaker::Product.product_name }
    price { rand(1..50) }
  end

  factory :invalid_product, parent: :product do
    description nil
    price nil
  end
end
