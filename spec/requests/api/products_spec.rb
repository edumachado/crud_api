require 'rails_helper'

describe 'Api::Products', type: :request do
  let(:product) { create :product }

  describe "GET 'index'" do
    it 'gives a success JSON response' do
      get api_v1_products_path

      expect(response).to be_success
    end

    it 'renders the JSON stories' do
      create_list :product, 3

      get api_v1_products_path
      expect(response).to match_response_schema('products/index')
    end
  end

  describe "GET 'show'" do
    it 'gives a success JSON response' do
      get api_v1_product_path(product.id)

      expect(response).to be_success
    end

    it 'renders the JSON story' do
      get api_v1_product_path(product.id)
      expect(response).to match_response_schema('products/show')
    end
  end

  describe '#create' do
    let(:product_params) { attributes_for(:product) }

    it 'persists a new Product record' do
      expect do
        post api_v1_products_path, product: product_params
      end.to change(Product, :count).by 1
    end

    it 'returns a serialized version of the new Product record' do
      post api_v1_products_path, product: product_params
      expect(response).to match_response_schema('products/show')
    end
  end

  describe '#update' do
    it 'updates the product record and returns the updated product record' do
      product = create(:product)
      patch api_v1_product_path(product.id), product: { description: 'new description' }
      expect(JSON.parse(response.body)['data']['attributes']['description']).to eq 'new description'
      expect(response).to match_response_schema('products/show')
    end
  end

  describe '#delete' do
    let!(:product) { create(:product) }

    it 'destroys the product record' do
      expect do
        delete api_v1_product_path(product.id)
      end.to change(Product, :count).by(-1)
    end

    it 'returns the destroyed product record' do
      delete api_v1_product_path(product.id)
      expect(response).to match_response_schema('products/show')
    end
  end
end
