require 'rails_helper.rb'

feature 'Creating product' do
  scenario 'can create a product' do
    fill_form_with('Car', '10')
    expect(page).to have_content('Car')
  end

  scenario 'can not create a product with negative price' do
    fill_form_with('Car', '-1')
    expect(page).to have_content('Price must be greater than or equal to 0')
  end
end
