require 'rails_helper'

describe ProductSerializer, type: :serializer do
  it 'creates special JSON for the API' do
    product = create(:product, id: 123, price: 10, description: 'description')

    serializer = ProductSerializer.new product

    json = '{"id":123,"description":"description","price":"10.0"}'
    expect(serializer.to_json).to eql(json)
  end
end
