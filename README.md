### Configuring

```sh-session
$ bundle install
$ rake db:setup
```

### Running

```sh-session
$ rails s
```

### Testing

The project uses RSpec and Capybara frameworks to test the system.

To run the test suite:

```sh-session
$ rspec
```
