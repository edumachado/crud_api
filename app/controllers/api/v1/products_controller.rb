module Api
  module V1
    class ProductsController < Api::ApplicationController
      before_action :find_product, only: [:show, :destroy, :update]

      def index
        products = Product.all
        render json: products, each_serializer: ProductSerializer
      end

      def create
        render json: Product.create(product_params)
      end

      def show
        render json: @product
      end

      def update
        @product.update_attributes(product_params)
        render json: @product
      end

      def destroy
        render json: @product.destroy
      end

      private

      def find_product
        @product = Product.find_by(id: params[:id])
      end

      def product_params
        params.require(:product).permit(:description, :price)
      end
    end
  end
end
