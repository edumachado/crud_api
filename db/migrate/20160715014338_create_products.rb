class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :description, null: false
      t.decimal :price, precision: 12, scale: 3, null: false

      t.timestamps null: false
    end
  end
end
